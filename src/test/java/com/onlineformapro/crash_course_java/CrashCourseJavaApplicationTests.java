package com.onlineformapro.crash_course_java;

import com.onlineformapro.crash_course_java.controllers.HomeController;
import com.onlineformapro.crash_course_java.jpa.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Iterator;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CrashCourseJavaApplicationTests {

	@Autowired
	private HomeController homeController;

	@Test
	void contextLoads() {
		assertThat(homeController).isNotNull();
	}

	@Test
	void controllerReturnsUsers() {
		Iterable<User> users = homeController.getAllUsers();

		int size = 0;
		for(Iterator<User> it = users.iterator(); it.hasNext(); it.next())
			size++;

		assert size == 5;
	}

}
