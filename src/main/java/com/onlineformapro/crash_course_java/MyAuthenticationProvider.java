package com.onlineformapro.crash_course_java;

import com.onlineformapro.crash_course_java.jpa.User;
import com.sun.istack.NotNull;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SecurityException;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static java.util.Optional.ofNullable;


/**
 * Simple JWT implementation of an AuthenticationProvider
 */
@Component
public class MyAuthenticationProvider implements AuthenticationProvider {

    // We need a signing key, so we'll create one just for this example. Usually
    // the key would be read from your application configuration instead.
    // This key MUST be kept private !!
    private static final Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);


    /**
     * Try to decode the token and authenticate the request if the token is valid and signed
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // We can cast to JWTTokenAuth as the provider only handle this class (see MyAuthenticationProvider::supports)
        JWTTokenAuth token = (JWTTokenAuth) authentication;

        try {
            Claims claims = token.validateToken();

            // Retrieve the user info from the claims
            Integer userId = Integer.valueOf(claims.getSubject());
            String firstName = claims.get("given_name", String.class);
            String lastName = claims.get("family_name", String.class);

            // We may use additional tests if required
            // ...

            // REQUIRED : Finally set the authentication object as authenticated
            authentication.setAuthenticated(true);
            return authentication;

        } catch(SecurityException e) {
            // Someone may be trying to forge a token...
            throw new BadCredentialsException("Invalid token signature", e);

        } catch(ExpiredJwtException e) {
            // Token expired, we may optionally reply to the client to ask him to renew its token
            throw new BadCredentialsException("Token expired", e);

        } catch(JwtException e) {
            // Other errors processing token, may be malformed
            throw new BadCredentialsException("Bad token", e);
        }
    }


    /**
     * Test if this authentication provider can handle the given authentication object
     * Our component can only handle our token implementation: 'JWTTokenAuth'
     */
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(JWTTokenAuth.class);
    }


    /**
     * Generate a JWT token for the given user
     *
     * @param user A user instance
     * @return An encoded JWT token with claims for user information and expiration date
     */
    public static String generateToken(User user) {
        Instant now = Instant.now();

        // Prepare additional claims for the user
        // List of registered claims : https://www.iana.org/assignments/jwt/jwt.xhtm
        Map<String, Object> userInfo = new HashMap<>();
        userInfo.put("given_name", user.getFirstName());
        userInfo.put("family_name", user.getLastName());

        return Jwts.builder()
                .setSubject(Integer.toString(user.getId()))
                .addClaims(userInfo)
                .setIssuedAt(Date.from(now))
                .setExpiration(Date.from(now.plus(1, ChronoUnit.HOURS)))
                .signWith(key)
                .compact();
    }


    /**
     * Simple implementation of 'AbstractAuthenticationToken' for JWT tokens
     */
    public static class JWTTokenAuth extends AbstractAuthenticationToken {
        private final String token;
        private Integer userId;


        /**
         * Create a new instance using the given token
         *
         * @param token An encoded JWT token
         */
        public JWTTokenAuth(String token) {
            super(null);
            this.token = token;
        }


        /**
         * Try to decode the token
         *
         * @return The token's claims
         */
        public Claims validateToken() {
            Jws<Claims> jws = Jwts.parserBuilder()
                    .setSigningKey(key)
                    .build()
                    .parseClaimsJws(token);

            this.userId = Integer.parseInt(jws.getBody().getSubject());
            return jws.getBody();
        }


        /**
         * Static helper to extract the token and create a JWTTokenAuth from it
         * @param authorizationHeader An 'Authorization: Bearer' string
         * @return An instance of JWTTokenAuth
         * @throws AuthenticationCredentialsNotFoundException
         */
        public static @NotNull JWTTokenAuth fromHeader(String authorizationHeader) {
            final String token = ofNullable(authorizationHeader)
                    .map(HeadersHelpers::getAuthToken)
                    .orElse(null);

            // Fail if there is no token
            if(token == null || token.isBlank())
                throw new AuthenticationCredentialsNotFoundException("Authentication token not found");

            return new JWTTokenAuth(token);
        }


        @Override
        public Object getPrincipal() {
            return this.userId;
        }

        @Override
        public Object getCredentials() {
            return this.token;
        }
    }
}
