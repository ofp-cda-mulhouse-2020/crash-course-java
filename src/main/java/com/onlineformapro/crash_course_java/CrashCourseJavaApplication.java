package com.onlineformapro.crash_course_java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrashCourseJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrashCourseJavaApplication.class, args);
	}

}
