package com.onlineformapro.crash_course_java;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;


/**
 * A RequestFilter for token authentication
 *
 * Responsible for extracting the token if present and ensuring its validity by requesting the associated user
 */
public class TokenAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    protected TokenAuthenticationFilter(RequestMatcher matcher) {
        super(matcher);
    }


    /**
     * Called to process a request through the filter
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        String header = request.getHeader(AUTHORIZATION);
        Authentication auth = MyAuthenticationProvider.JWTTokenAuth.fromHeader(header);
        return getAuthenticationManager().authenticate(auth);
    }


    /**
     * Overridden to ensure the filter chain continue to be processed.
     * Without it Spring will stop the chain and return an HTTP Status OK (200) without continuing to the REST controller
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        chain.doFilter(request, response);
    }

}
