package com.onlineformapro.crash_course_java.controllers;

import com.onlineformapro.crash_course_java.jpa.User;
import com.onlineformapro.crash_course_java.jpa.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.*;


@RestController
public class UsersController extends AbstractCrudRestController<User> {

    @Autowired
    private UserRepository userRepository;

    public UsersController() {
        super("/users", "user");
    }

    @Override
    protected CrudRepository<User, Integer> getEntitiesRepository() {
        return userRepository;
    }

    @GetMapping("/users")
    public Iterable<User> getUsers() {
        return super.getEntities();
    }

    @GetMapping("/users/{userId}")
    public User getUser(@PathVariable int userId) {
        return super.getEntity(userId);
    }

    @DeleteMapping("/users/{userId}")
    public void deleteUser(@PathVariable int userId) {
        super.deleteEntity(userId);
    }

    @PostMapping(path = "/users", headers = "Accept=application/json")
    public User createEntity(@RequestBody User user) {
        return super.createEntity(user);
    }

    @PutMapping(path = "/users/{userId}", headers = "Accept=application/json")
    public User updateUser(@PathVariable int userId, @RequestBody User user) {
        return super.updateEntity(userId, user);
    }

}
