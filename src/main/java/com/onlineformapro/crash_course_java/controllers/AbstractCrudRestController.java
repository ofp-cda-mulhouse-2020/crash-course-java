package com.onlineformapro.crash_course_java.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * Generic class for CRUD Rest controllers
 * @param <T> The type of entity used in the controller
 */
public abstract class AbstractCrudRestController<T extends AbstractCrudRestController.IdentifiableEntity> {

    @Autowired
    private SimpMessagingTemplate websocketMessage;

    /**
     * Base name for websocket events
     */
    private final String webSocketEventBaseName;

    /**
     * Websocket topic
     */
    private final String webSocketTopic;



    /**
     * Default constructor
     *
     * @param webSocketTopic The websocket topic to use for dispatching events
     * @param webSocketEventBaseName The base name for websocket events (BASE_NAME_created, BASE_NAME_updated and BASE_NAME_deleted)
     */
    public AbstractCrudRestController(String webSocketTopic, String webSocketEventBaseName) {
        this.webSocketTopic = webSocketTopic;
        this.webSocketEventBaseName = webSocketEventBaseName;
    }


    /**
     * Abstract method used to get the repository from implementations
     */
    protected abstract CrudRepository<T, Integer> getEntitiesRepository();



    /**
     * List all the entities from the repository
     *
     * @return An iterable collection of entities
     */
    public Iterable<T> getEntities() {
        return getEntitiesRepository().findAll();
    }


    /**
     * Get an entity by its ID
     *
     * @param id The integer ID of the entity to look for
     * @return An entity
     * @throws NotFoundException When no entity with this ID is found
     */
    public T getEntity(int id) {
        return getEntitiesRepository().findById(id).orElseThrow(NotFoundException::new);
    }


    /**
     * Delete an entity by its ID
     *
     * @param id The integer ID of the entity to delete
     */
    public void deleteEntity(int id) {
        getEntitiesRepository().deleteById(id);
        websocketMessage.convertAndSend(webSocketTopic, new EntityDeletedEvent(webSocketEventBaseName, id));
    }


    /**
     * Create a new entity
     *
     * @param entity An entity instance as popuplated by its JSON counterpart
     * @return A new entity instance, as returned by JPA repository.save method
     */
    public T createEntity(T entity) {
        // Override the ID to force JPA to insert a value and not update an existing one
        entity.setId(null);

        T newEntity = getEntitiesRepository().save(entity);
        websocketMessage.convertAndSend(webSocketTopic, new EntityCreatedEvent(webSocketEventBaseName, newEntity));
        return newEntity;
    }


    /**
     * Update an existing entity
     *
     * @param id The integer ID of the entity to update
     * @param entity An entity instance as popupalted by its JSON counterpart
     * @return A new entity instance, as returned by JPA repository.save method
     * @throws NotFoundException When no entity with this ID is found
     */
    public T updateEntity(int id, T entity) {
        // First try to fetch the entity to validate its presence
        getEntity(id);

        // Override the received user to force the id as received on the url
        entity.setId(id);

        T updatedEntity = getEntitiesRepository().save(entity);
        websocketMessage.convertAndSend(webSocketTopic, new EntityUpdatedEvent(webSocketEventBaseName, updatedEntity));
        return updatedEntity;
    }


    /**
     * Generic interface for entities used to set and get the id
     */
    public interface IdentifiableEntity {
        Integer getId();
        void setId(Integer id);
    }


    /**
     * Dedicated implementation of 'RuntimeException' to generate an HTTP Not Found error
     */
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    static class NotFoundException extends RuntimeException {}


    /**
     * WebSocket event EntityDeleted
     */
    static class EntityDeletedEvent {
        @SuppressWarnings("unused")
        public final String event;
        public final int userId;

        public EntityDeletedEvent(String wsBaseName, int userId) {
            this.event = String.format("%s_deleted", wsBaseName);
            this.userId = userId;
        }
    }

    /**
     * Abstract class for WebSocket event with entity details
     */
    abstract class EntityDetailsEvent {
        public String event;
        public T user;

        public EntityDetailsEvent(String event, T entity) {
            this.event = event;
            this.user = entity;
        }
    }

    /**
     * WebSocket event EntityUpdated
     */
    class EntityUpdatedEvent extends EntityDetailsEvent {
        public EntityUpdatedEvent(String wsBaseName, T entity) {
            super(String.format("%s_updated", wsBaseName), entity);
        }
    }

    /**
     * WebSocket event EntityCreated
     */
    class EntityCreatedEvent extends EntityDetailsEvent {
        public EntityCreatedEvent(String wsBaseName, T entity) {
            super(String.format("%s_created", wsBaseName), entity);
        }
    }
}
