package com.onlineformapro.crash_course_java.controllers;

import com.onlineformapro.crash_course_java.MyAuthenticationProvider;
import com.onlineformapro.crash_course_java.jpa.User;
import com.onlineformapro.crash_course_java.jpa.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


/**
 * Controller dedicated to authentication
 */
@RestController
public class LoginController {

    @Autowired
    UserRepository userRepository;


    /**
     * Login API call, take a pair of firstName/email to authenticate
     *
     * @param firstName The user's first name
     * @param email The user's email
     * @return Either a token if the authentication is successful, or an HTTP 403 status
     */
    @PostMapping("/login")
    public String login(
            @RequestParam(name = "firstName") String firstName,
            @RequestParam(name = "email") String email
    ) {
        // Try to find a user with the given information
        User user = userRepository.findByFirstNameAndEmail(firstName, email);

        // HTTP forbidden if no user is found
        if(user == null)
            throw new ForbiddenException();

        // Returns a new token when the authentication is successful
        return MyAuthenticationProvider.generateToken(user);
    }


    /**
     * Logout API call
     */
    @PostMapping("/logout")
    public void logout() {
        // Nothing to do in here, the logout should be handled by
        // the client simply by forgetting the token
        // Ultimately this API endpoint may be entirely removed
    }


    /**
     * Dedicated implementation of 'RuntimeException' to generate HTTP Forbidden error
     */
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    static class ForbiddenException extends RuntimeException {}

}
