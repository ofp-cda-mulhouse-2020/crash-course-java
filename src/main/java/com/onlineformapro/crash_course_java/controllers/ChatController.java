package com.onlineformapro.crash_course_java.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

import java.security.Principal;
import java.time.Instant;

@Controller
public class ChatController {

    /**
     * Handle incoming messages, basic String version, the JSON decoding is done manually
     */
    /*
    @MessageMapping("/chatroom.send")
    @SendTo("/chatroom")
    public String handleIncoming(Principal principal, String message) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            IncomingMessage incoming = mapper.readValue(message, IncomingMessage.class);

            ChatMessage outgoing = new ChatMessage(principal.getName(), incoming.msg);
            String json = mapper.writeValueAsString(outgoing);
            return json;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }
    */


    /**
     * Handle incoming message using Jackson autobinding from JSON to Java object
     * Great when it works but errors decoding the JSON are never handled. When debugging
     * those errors, try the String version above, no autobinding problems !
     * @param principal
     * @param incoming
     * @return
     */
    @MessageMapping("/chatroom.send")
    @SendTo("/chatroom")
    public ChatMessage handleIncomingJackson(Principal principal, IncomingMessage incoming) {
        return new ChatMessage(principal.getName(), incoming.msg);
    }

    @SuppressWarnings("unused")
    static class IncomingMessage {
        public String msg;
    }

    @SuppressWarnings("unused")
    static class ChatMessage {
        public long timestamp;
        public String user;
        public String msg;

        public ChatMessage(String user, String msg) {
            this.user = user;
            this.msg = msg;
            this.timestamp = Instant.now().toEpochMilli();
        }
    }

}
