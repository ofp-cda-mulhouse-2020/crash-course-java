package com.onlineformapro.crash_course_java.jpa;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {

    // New method to find user by firstName and email
    // auto implemented by JPA according to the name of the method
    User findByFirstNameAndEmail(String firstName, String email);

}
