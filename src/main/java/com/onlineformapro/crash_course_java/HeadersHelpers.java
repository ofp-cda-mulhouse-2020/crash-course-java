package com.onlineformapro.crash_course_java;

/**
 * Helpers for HTTP headers
 */
public class HeadersHelpers {

    private static final String BEARER = "Bearer";


    /**
     * Remove the prefix of a string if present
     *
     * @param authorizationHeader The HTTP header 'Authorization'
     * @return The extracted token
     */
    public static String getAuthToken(String authorizationHeader) {
        if(authorizationHeader.startsWith(BEARER))
            return authorizationHeader.substring(BEARER.length()).trim();
        else
            return authorizationHeader;
    }

}
