package com.onlineformapro.crash_course_java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;


/**
 * Main configuration object for Spring Security (could be written using xml instead, but will be even more painful)
 */
@Configuration
@EnableWebSecurity
public class SecurityManager extends WebSecurityConfigurerAdapter {

    // URLs which don't require authentication
    RequestMatcher public_urls = new OrRequestMatcher(
            new AntPathRequestMatcher("/login"),
            new AntPathRequestMatcher("/websocket/**"),
            new AntPathRequestMatcher("/hello")
    );


    // Inject the authentication manager component
    @Autowired
    private MyAuthenticationProvider authProvider;


    /*
     * Setup the authentication manager
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider);
    }


    /*
     * Configure the HTTP stack of Spring Security
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                // Enable CORS support
                .cors().and()

                // Disable session for REST services
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()

                // Define global authorization
                .authenticationProvider(authProvider)
                .addFilterBefore(restAuthenticationFilter(), BasicAuthenticationFilter.class)
                .authorizeRequests()
                .requestMatchers(public_urls).permitAll()
                .anyRequest().authenticated()
                .and()

                // Disable built-in mechanisms for authentication
                .csrf().disable()
                .httpBasic().disable()
                .formLogin().disable()
                .logout().disable();
    }


    /**
     * This method setup the filter responsible for detecting the authorization token :
     * - the filter only process routes which are not 'public'
     * - the authentication manager is passed to the filter (needed by the filter implementation)
     * - the success handler is overridden to prevent redirection
     */
    @Bean
    TokenAuthenticationFilter restAuthenticationFilter() throws Exception {
        final TokenAuthenticationFilter filter = new TokenAuthenticationFilter(new NegatedRequestMatcher(public_urls));
        filter.setAuthenticationManager(authenticationManager());
        filter.setAuthenticationSuccessHandler(successHandler());
        return filter;
    }



    /**
     * This method setup a no-redirect strategy to prevent Spring from redirecting the HTTP client
     * when an authentication is successful. Without it, the 'login' call returns a HTTP 302 redirect
     * instead of the result of the login controller
     */
    @Bean
    SimpleUrlAuthenticationSuccessHandler successHandler() {
        final SimpleUrlAuthenticationSuccessHandler successHandler = new SimpleUrlAuthenticationSuccessHandler();
        successHandler.setRedirectStrategy(new NoRedirectStrategy());
        return successHandler;
    }


    /**
     * Configure the CORS filter to allow from everywhere
     */
    @Bean
    CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();

        corsConfiguration.setAllowCredentials(true);
        corsConfiguration.setAllowedOriginPatterns(Arrays.asList("*"));
        corsConfiguration.setAllowedMethods(Arrays.asList(CorsConfiguration.ALL));
        corsConfiguration.setAllowedHeaders(Arrays.asList(CorsConfiguration.ALL));
        source.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsFilter(source);
    }

}


/**
 * This class implements 'RedirectStrategy' to disable redirects
 */
class NoRedirectStrategy implements RedirectStrategy {
    @Override
    public void sendRedirect(final HttpServletRequest request, final HttpServletResponse response, final String url) throws IOException {
        // No redirect is required with pure REST
    }
}
