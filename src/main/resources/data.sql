INSERT INTO user(first_name, last_name, email) VALUES
  ('Thomas', 'Dacquin', 't.dascquin@gmail.com'),
  ('Yves', 'Dutronc', 'yves.dutronc@tutanota.com'),
  ('Mathieu', 'François', 'mathieu.francois@tutanota.com'),
  ('Bertrand', 'Mouget', 'bertrand.mouget@tutanota.com'),
  ('Françoise', 'Giroud', 'f.giroud@onlineformapro.com');
