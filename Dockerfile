FROM openjdk:17-jdk-alpine
COPY ./build/libs/crash_course_java-0.0.1-SNAPSHOT.jar /
CMD java -jar /crash_course_java-0.0.1-SNAPSHOT.jar
